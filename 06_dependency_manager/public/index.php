<?php

error_reporting(-1);
ini_set("display_errors", "On");


require "../autoload.php";

Config\Directory::set(__DIR__ . "/../");

$app = new App();
$app->run();


$parts = explode('/', $_SERVER['REQUEST_URI']);
array_shift($parts);

$users = [
    1 => [
        'name' => 'John',
        'surname' => 'Doe',
        'age' => 21
    ],
    2 => [
        'name' => 'Peter',
        'surname' => 'Bauer',
        'age' => 16
    ],
    3 => [
        'name' => 'Paul',
        'surname' => 'Smith',
        'age' => 18
    ]
];

$option = $parts[0] ?? 'home';

if ($option == '' ) {

    $view = 'home';

} else if ($option == 'home' ) {

    $view = 'home';

} else if ($option == 'about') {

    $view = 'about';

} else if ($option == 'users') {


    if (isset($parts[1])) {

        $user = $users[$parts[1]];
        $view = 'user';
    } else {

        $view = 'users';
    }

} else {

    $view = '404';
}

require_once('../app/view/layout.php');

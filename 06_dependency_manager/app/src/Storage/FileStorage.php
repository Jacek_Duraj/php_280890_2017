<?php

namespace Storage;

use Concept\Distinguishable;
use Config\Directory;

class FileStorage implements Storage
{
    public function store(Distinguishable $distinguishable)
    {
        $directory = Directory::storage() . "files/";

        file_put_contents(
            $directory . $distinguishable->key(),
            serialize($distinguishable)
        );
    }

    public function loadAll(): array
    {
        $ignored = ['..', '.', '.gitignore'];
        $directory = Directory::storage() . "files/";

        $result = [];

        $files = array_diff(scandir($directory), $ignored);

        foreach ($files as $file) {
            $result[] = unserialize(file_get_contents($directory . $file));
        }

        return $result;
    }
}
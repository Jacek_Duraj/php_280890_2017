# Dependency Manager - Composer

## How to setup server?

```

# Switch to root account
sudo su -

# Go to Nginx configuration directory
cd /etc/nginx/sites-available/

# Copy example configuration from project directory
cp /home/student/Phpstorm/06_dependency_manager/nginx/06_dependency_manager .

# Go to enabled configuration directory
cd /etc/nginx/sites-enabled/

# Enable new configuration
ln -s /etc/nginx/sites-available/06_dependency_manager .

# Restart server
service nginx restart

# Test whether it works in browser or using e.g.: curl
curl localhost:8006

# You should see some HTML in console output

# Exeit from root session
exit
```

## How to setup storage permissions?

```

chmod -R 0777 storage/
```


<?php

use Widget\Widget;

class Scene
{
    public function render(Widget $widget)
    {
        $widget->draw();
    }
}
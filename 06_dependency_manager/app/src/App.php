<?php

use Widget\Link;
use Widget\Button;

use Storage\Storage;
use Storage\SessionStorage;
use Storage\FileStorage;
use Storage\SQLiteStorage;
use Storage\MySQLStorage;

class App {

    public function run()
    {
        echo "<h2>Session Storage Demo</h2>";

        $this->demo(new SessionStorage());

        echo "<h2>File Storage Demo</h2>";

        $this->demo(new FileStorage());

        echo "<h2>SQLite Storage Demo</h2>";

        $this->demo(new SQLiteStorage());

        echo "<h2>MySQL Storage Demo</h2>";

        $this->demo(new MySQLStorage());

        echo "<hr>";
    }

    public function demo(Storage $storage)
    {
        $storage->store(new Button(1));
        $storage->store(new Button(2));
        $storage->store(new Link(3));

        $widgets = $storage->loadAll();

        $scene = new Scene();
        foreach ($widgets as $widget) {
            $scene->render($widget);
        }
    }
}
